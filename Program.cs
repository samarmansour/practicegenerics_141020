﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    class Program
    {
        static void Main(string[] args)
        {
            MyMath myMath = new MyMath();
            Console.WriteLine(myMath.GetMax(25, 65));
            Console.WriteLine(myMath.GetMax("a", "g"));

            AccountDollar dollar = new AccountDollar();
            AccountEuro euro = new AccountEuro();
            AccountShekel shakel = new AccountShekel();

            Console.WriteLine("================Dollar=================");
            Console.WriteLine($"Dollar to euro: {dollar.ConverToEuro((Dollar)6.25).Balance}");
            Console.WriteLine($"Dollar to Shakel: {dollar.ConvertToShakel((Dollar)10).Balance}");
            Console.WriteLine($"Dollar to Dollar: {dollar.ConverToUSD((Dollar)5).Balance}");
            
            Console.WriteLine("================Euro=================");
            Console.WriteLine($"Euro to Euro: {euro.ConverToEuro((Euro)50).Balance}");
            Console.WriteLine($"Euro to Shakel: {euro.ConvertToShakel((Euro)20).Balance}");
            Console.WriteLine($"Euro to Dollar: {euro.ConverToUSD((Euro)10).Balance}");
            
            Console.WriteLine("================Shekel=================");
            Console.WriteLine($"Shekel to Euro: {shakel.ConverToEuro((Shekel)100).Balance}");
            Console.WriteLine($"Shekel to Shakel: {shakel.ConvertToShakel((Shekel)20).Balance}");
            Console.WriteLine($"Shekel to Dollar: {shakel.ConverToUSD((Shekel)50).Balance}");


        }
    }
}
