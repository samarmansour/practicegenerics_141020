﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    class MyMath 
    {
        public T GetMax<T>(T t1, T t2) where T : IComparable
        {
            return t1.CompareTo(t2) > 0 ? t1 : t2;
        }

        // two ways to return null value:
        // 1. T : class -- allows to return null
        public T GetMaxOrNull<T>(T t1, T t2) where T : class, IComparable
        {
            int result = t1.CompareTo(t2);

            if (result == 0)
                return null;
            if (result > 0)
                return t1;
            return t2;
        }

        // 2. default (T) -- allows to return null
        public T GetMaxOrNull2<T>(T t1, T t2) where T : IComparable
        {
            int result = t1.CompareTo(t2);

            if (result == 0)
                return default(T);
            if (result > 0)
                return t1;
            return t2;
        }
    }
}
