﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    class AccountShekel : Account<Shekel>, IComparable<AccountShekel>
    {
        public int CompareTo(AccountShekel other)
        {
            return balance.Balance.CompareTo(other.balance.Balance);
        }

        public override Euro ConverToEuro(Shekel num)
        {
            if (num.Balance == 0)
                return (Euro)0;
            if (num.Balance < 0)
                throw new ArgumentException("connot convert negative balance!!");
            return (Euro)(num.Balance / 4.5);
        }

        public override Dollar ConverToUSD(Shekel num)
        {
            if (num.Balance == 0)
                return (Dollar)0;
            if (num.Balance < 0)
                throw new ArgumentException("connot convert negative balance!!");
            return (Dollar)(num.Balance / 3.5);
        }

        public override Shekel ConvertToShakel(Shekel num)
        {
            return num;
        }

    }
}
