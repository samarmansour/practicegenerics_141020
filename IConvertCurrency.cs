﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    interface IConvertCurrency<T> where T : Currency
    {
        Dollar ConverToUSD(T num);
        Shekel ConvertToShakel(T num);
        Euro ConverToEuro(T num);
    }
}
