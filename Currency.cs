﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    class Currency
    {
        public double Balance { get; set; }
    }

    class Shekel : Currency
    {
        public static explicit operator Shekel(double dbl)
        {
            Shekel shakel = new Shekel();
            shakel.Balance = dbl;
            return shakel;
        }
    }

    class Dollar  : Currency
    {
        public static explicit operator Dollar(double dbl)
        {
            Dollar dollar = new Dollar();
            dollar.Balance = dbl;
            return dollar;
        }
    }

    class Euro : Currency
    {
        public static explicit operator Euro(double dbl)
        {
            Euro euro = new Euro();
            euro.Balance = dbl;
            return euro;
        }
    }
}


