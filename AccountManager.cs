﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    class AccountManager
    {
        public static Account<T> GetMax<T>(Account<T> t1, Account<T> t2) where T : Currency, IComparable
        {
            return t1.balance.CompareTo(t2.balance) > 0 ? t1 : t2;
        }
    }
}
