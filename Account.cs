﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    abstract class Account<T> : IConvertCurrency<T> where T : Currency
    {
        public T balance;
        
        public abstract Euro ConverToEuro(T num);

        public abstract Dollar ConverToUSD(T num);

        public abstract Shekel ConvertToShakel(T num);
    }
}
