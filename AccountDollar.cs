﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    class AccountDollar : Account<Dollar>, IComparable<AccountDollar>
    {
        public int CompareTo(AccountDollar other)
        {
            return balance.Balance.CompareTo(other.balance.Balance);
        }

        public override Euro ConverToEuro(Dollar num)
        {
            if (num.Balance < 0)
                throw new ArgumentException("Cannot convert negative curreny");
            if (num.Balance == 0)
                return (Euro)0;
            return (Euro)(num.Balance * 0.85);
        }

        public override Dollar ConverToUSD(Dollar num)
        {
            return num;
        }

        public override Shekel ConvertToShakel(Dollar num)
        {
            if (num.Balance < 0)
                throw new ArgumentException("Cannot convert negative curreny");
            if (num.Balance == 0)
                return (Shekel)0;
            return (Shekel)(num.Balance * 3.39);
        }
    }
}
