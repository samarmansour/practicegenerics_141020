﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _141020
{
    class AccountEuro : Account<Euro>, IComparable<AccountEuro>
    {
        public int CompareTo(AccountEuro other)
        {
            return balance.Balance.CompareTo(other.balance.Balance);
        }

        public override Euro ConverToEuro(Euro num)
        {
            return num;
        }

        public override Dollar ConverToUSD(Euro num)
        {
            if (num.Balance == 0)
                return (Dollar)0;
            if (num.Balance < 0)
                throw new ArgumentException("Cannot convert negative currency!!");
            return (Dollar)(num.Balance * 1.17);
        }

        public override Shekel ConvertToShakel(Euro num)
        {
            if (num.Balance == 0)
                return (Shekel)0;
            if (num.Balance < 0)
                throw new ArgumentException("Cannot convert negative currency!!");
            return (Shekel)(num.Balance * 3.97);
        }
    }
}
